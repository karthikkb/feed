package com.example.feeds

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.feeds.activity.MainViewModel
import com.example.feeds.model.Data
import com.example.feeds.model.ResponseResult
import com.example.feeds.repository.Remote
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.stubbing.OngoingStubbing
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

@RunWith(MockitoJUnitRunner::class)
class MainViewModelUnitTest {

    @Rule @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var repo : Remote

    @Mock
    lateinit var errorMessageObserver : androidx.lifecycle.Observer<String>

    @Mock
    lateinit var mResponseResultObserver : androidx.lifecycle.Observer<ResponseResult>

    val vm = MainViewModel()
    lateinit var dummyResponseResult : ResponseResult

    @Before
     fun setup(){
        val data = ArrayList<Data>()
        data.add(Data("canada","canadaDes","canadaImage"))
        dummyResponseResult = ResponseResult("canada",data)
        vm.errorMessage.observeForever(errorMessageObserver)
        vm.mResponseResult.observeForever(mResponseResultObserver)
     }

    @Test
    fun testDataList(){
        Mockito.`when`(vm.getDataByApi()).thenReturn(dummyResponseResult)
        assert(vm.mResponseResult.value!!.title!=null)
    }

    @Test
    fun testDataListFail(){
        Mockito.`when`(vm.getDataByApi()).thenReturn(throw Exception("fail"))
        assert(vm.errorMessage.value!=null)
    }

    @After
    fun tearDown(){
        vm.errorMessage.removeObserver(errorMessageObserver)
        vm.mResponseResult.removeObserver(mResponseResultObserver)
    }
}

private fun <T> OngoingStubbing<T>.thenReturn(dummyResponseResult: ResponseResult) {

}
