package com.example.feeds.repository

import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.common_util.util.network.NetworkHelper
import com.example.common_util.utils.Logger
import com.example.feeds.BuildConfig
import com.example.feeds.MyApplication
import com.example.feeds.model.ResponseResult

object Remote {
    fun getDataByApi(apiCallBack : ApiCallBack){
            AndroidNetworking.get(BuildConfig.BASE_URL)
                .setTag("getData")
                .setPriority(Priority.HIGH)
                .build()
                .getAsObject(
                    ResponseResult::class.java,
                    object : ParsedRequestListener<ResponseResult> {
                        override fun onResponse(response: ResponseResult?) {
//                            Logger.d("data response", "" + Gson().toJson(response))
                            response?.let {
                                Logger.d("data", "" + it.rows.size)
                                val data = it.rows.filter { it.title != null }
                                Logger.d("data", "" + data.size)
                                apiCallBack.onSuccess(it)
                            }

                        }

                        override fun onError(anError: ANError?) {
                            Logger.d("data anError", "" + anError!!.message)
                            apiCallBack.onFailure(""+anError!!.message)
                        }

                    })
        }

    interface ApiCallBack{
        fun onSuccess(response : Any)
        fun onFailure(error : String)
    }

}