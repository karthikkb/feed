package com.example.feeds.model

data class ResponseResult (
    var title : String,
    var rows : List<Data>
)