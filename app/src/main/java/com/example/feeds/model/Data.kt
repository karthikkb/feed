package com.example.feeds.model



data class Data (
    val title: String?,
    val description: String?,
    val imageHref: String?
    ){

}