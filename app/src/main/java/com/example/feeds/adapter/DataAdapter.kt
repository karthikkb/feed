package com.example.feeds.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.feeds.MyApplication
import com.example.feeds.R
import com.example.feeds.activity.MainViewModel
import com.example.feeds.databinding.ListItemBinding
import com.example.feeds.model.Data

class DataAdapter(val mainViewModel: MainViewModel, var datas: List<Data>) : RecyclerView.Adapter<DataAdapter.MyViewHolder>() {

    private var layoutInflater: LayoutInflater? = null

    class MyViewHolder(val binding: ListItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding = DataBindingUtil.inflate<ListItemBinding>(
                layoutInflater!!,
                R.layout.list_item,
                parent,
                false
            )
            return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = datas.get(position)

        //setting image with glide library
        data.imageHref?.let {
            Glide.with(MyApplication.myAppContext).load(data.imageHref)
                .placeholder(R.mipmap.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.DATA)
                .into(holder.binding.img)
        }

        data.title?.let {
            holder.binding.tvTitle.setText(it) }
        data.description?.let {
            holder.binding.tvDes.setText(it)
        }

    }

}