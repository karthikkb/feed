package com.example.feeds

import android.app.Application
import com.androidnetworking.AndroidNetworking
import timber.log.Timber


class MyApplication : Application() {

    companion object {
        lateinit var myAppContext: MyApplication
    }

    override fun onCreate() {
        super.onCreate()
        myAppContext = this

        AndroidNetworking.initialize(this)

        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())

    }

}
