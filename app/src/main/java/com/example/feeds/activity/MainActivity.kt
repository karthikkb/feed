package com.example.feeds.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.ActionBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.common_util.base.BaseActivity
import com.example.feeds.R
import com.example.feeds.adapter.DataAdapter
import com.example.feeds.databinding.ActivityMainBinding
import com.example.feeds.model.ResponseResult

class MainActivity : BaseActivity<ActivityMainBinding,MainViewModel>() {

    lateinit var actionbar: ActionBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //action bar initialization
        actionbar = supportActionBar!!

        //recyclerview initialization
        mViewDataBinding.rv.setHasFixedSize(true)
        mViewDataBinding.rv.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL, false
        )

        //loader observer
        mViewModel.isLoading.observe(this, Observer {
                mViewDataBinding.swipeRefresh.isRefreshing = it
        })

        //error message observer
        mViewModel.errorMessage.observe(this, Observer {
            toastMessage(it)
        })

        // data list observer
        mViewModel.mResponseResult.observe(this, Observer { response ->
            loadData(response)
        })

        //call api to get Data
        if(viewModel.mResponseResult.value==null)
            viewModel.getDataByApi()
        else
            loadData(viewModel.mResponseResult.value!!)

        mViewDataBinding.swipeRefresh.setOnRefreshListener {
            viewModel.getDataByApi()
        }
    }

    fun loadData(response : ResponseResult) {
    actionbar.apply{ title = "${response.title} " }
    mViewDataBinding.rv.visibility = (View.VISIBLE)
    mViewDataBinding.rv.smoothScrollToPosition(0)
    val adapter = DataAdapter(mViewModel, response.rows)
    mViewDataBinding.rv.adapter = adapter
}

    override val bindingVariable: Int
        get() = -1
    override val layoutId: Int
        get() = R.layout.activity_main
    override val viewModel: MainViewModel
        get() = ViewModelProvider(this).get(MainViewModel::class.java)

}
