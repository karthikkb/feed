package com.example.feeds.activity

import androidx.lifecycle.MutableLiveData
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.common_util.base.BaseViewModel
import com.example.common_util.util.network.NetworkHelper
import com.example.common_util.utils.Logger
import com.example.feeds.BuildConfig
import com.example.feeds.MyApplication
import com.example.feeds.model.ResponseResult
import com.example.feeds.repository.Remote
import com.google.gson.Gson

class MainViewModel : BaseViewModel() {
//    val disposable : Job?=null

    var isLoading = MutableLiveData<Boolean>()
    var mResponseResult = MutableLiveData<ResponseResult>()
    var errorMessage: MutableLiveData<String> = MutableLiveData()

    init {
        mResponseResult
    }

    fun getDataByApi(){
            if(NetworkHelper.isInternetAvailable(MyApplication.myAppContext)) {
                isLoading.value = true
                Remote.getDataByApi(object : Remote.ApiCallBack {
                    override fun onSuccess(response: Any) {
                        mResponseResult.value = response as ResponseResult
                        isLoading.value = false
                    }

                    override fun onFailure(error: String) {
                        errorMessage.value = error
                        isLoading.value = false
                    }

                })
            }
            else
                toastMessageString.value = "No internet"
        }


    override fun onCleared() {
        super.onCleared()
    }

}
